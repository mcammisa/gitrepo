# See here for image contents: https://github.com/microsoft/vscode-dev-containers/tree/v0.205.1/containers/ubuntu/.devcontainer/base.Dockerfile

# [Choice] Ubuntu version (use hirsuite or bionic on local arm64/Apple Silicon): hirsute, focal, bionic
#ARG VARIANT="hirsute"
#FROM mcr.microsoft.com/vscode/devcontainers/base:0-${VARIANT}

FROM gitpod/workspace-full 
RUN wget -q -O - https://raw.githubusercontent.com/rancher/k3d/main/install.sh | bash
RUN wget https://github.com/stackrox/kube-linter/releases/download/0.2.5/kube-linter-linux.tar.gz && tar -xvf kube-linter-linux.tar.gz && chmod ugo+x kube-linter && mv kube-linter /usr/local/bin/
RUN curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.21.0

ARG HELM_VERSION="3.5.4"
ARG CT_VERSION=3.3.1
ENV DEBIAN_FRONTEND=noninteractive

RUN \
  apt-get update \
  && \
  apt-get -y install --no-install-recommends \
    libonig-dev \
    gnupg2 \
    python3-pip \
    python3-setuptools \
  && \
  sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d -b /usr/local/bin \
  && \
  curl -o /tmp/ct.tar.gz -L "https://github.com/helm/chart-testing/releases/download/v${CT_VERSION}/chart-testing_${CT_VERSION}_linux_$(dpkg --print-architecture).tar.gz" \
  && mkdir -p /etc/ct \
  && tar xvzf /tmp/ct.tar.gz -C /usr/local/bin "ct" \
  && tar xvzf /tmp/ct.tar.gz --strip-components=1 -C /etc/ct "etc/" \
  && chmod +x /usr/local/bin/ct \
  && \
  pip3 install \
    pre-commit \
    yamale \
    yamllint \
  && \
  wget -q -O /usr/local/bin/hadolint https://github.com/hadolint/hadolint/releases/download/v2.8.0/hadolint-Linux-x86_64 \
  && chmod ugo+x /usr/local/bin/hadolint \
  && \
  rm \
    /tmp/ct.tar.gz
